﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AbsarLib.DAL;
using AbsarLib.Entity;
using MVC.Models;
using System.Data.Entity;
using PagedList;
using AbsarLib.ServiceFolder;
namespace MVC.Controllers
{
    public class PersonController : Controller
    {
        //
        // GET: /Person/

        Service myService = new Service();

        //public ViewResult Index(String sortOrder, string currentFilter, String searchString, int? page)
        public ViewResult Index(HomeModel model)
        {

            if (model.SearchString != null)
            {
                model.Page = 1;
            }
            else
            {
                model.SearchString = model.CurrentFilter;
            }

            ViewBag.CurrentFilter = model.SearchString;

            model.CurrentSort = model.SortOrder;
            model.NameSortParm = String.IsNullOrEmpty(model.SortOrder) ? "name_desc" : "";
            //model.Persons = myService.dbSevicecontext.Persons.Include(i => i.Hobbies);
            //viewModel.Hobbies = viewModel.Persons.Where(i => i.ID == 1).Single().Hobbies;
            var query = myService.Search(model.SearchString).Select(m => new PersonOutModel (){ 
                ID = m.ID,
                FirstName = m.FirstName,
                SurName = m.SurName,
                Address = m.Address,
                Gender = m.Gender,
                City = m.City.Name,
                Phoneno = m.Phoneno,
                Email = m.Email,
                Hobbies = m.Hobbies.Select(h => h.Name).ToList()
            });

            switch (model.SortOrder)
            {
                case "name_desc":
                    query = query.OrderByDescending(s => s.FirstName);
                    break;
                default:
                    query = query.OrderBy(s => s.FirstName);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (model.Page ?? 1);
            model.Persons = query.ToPagedList(pageNumber, pageSize);

            return View(model);
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            populatedropdown();
            populatelist();
            //var persons = from t in dbcontext.Persons

            //               where t.ID == id
            //               select t;

            //PersonModel viewobj = new PersonModel();
            //foreach (var item in persons)
            //{
            //    viewobj.FirstName = item.FirstName;
            //    viewobj.Address = item.Address;
            //    viewobj.Cityid = item.CityID;
            //    viewobj.Email = item.Email;
            //    viewobj.Hobbies = item.Hobbies.ToString();
            //    viewobj.Phoneno = item.Phoneno;
            //    viewobj.SurName = item.SurName;
            //} 
            var person = myService.dbSevicecontext.Persons.Find(id);

            PersonModel model = new PersonModel();
            model.Hobbieslist = new List<int>();
            model.FirstName = person.FirstName;
            model.Gender = person.Gender;
            model.Address = person.Address;
            model.Cityid = person.CityID;
            model.Email = person.Email;
            model.SurName = person.SurName;
            model.Phoneno = person.Phoneno;
            foreach (var hobby in person.Hobbies)
            {
                model.Hobbieslist.Add(hobby.ID);
            }
            return View(model);
        }
        [ValidateAntiForgeryToken()]
        [HttpPost, ActionName("Edit")]
        public ActionResult EditPost(int id, PersonModel model)
        {
            populatedropdown();
            populatelist();
            if (ModelState.IsValid)
            {
                var personToUpdate = myService.dbSevicecontext.Persons.Find(id);
                personToUpdate.FirstName = model.FirstName;
                personToUpdate.SurName = model.SurName;
                personToUpdate.Gender = model.Gender;
                personToUpdate.CityID = model.Cityid;
                personToUpdate.Address = model.Address;
                personToUpdate.Email = model.Email;
                personToUpdate.Phoneno = model.Phoneno;
                TryUpdateModel(personToUpdate);


                try
                {

                    //UpdatePersonHobbies(model.Hobbieslist, personToUpdate);

                    myService.dbSevicecontext.Entry(personToUpdate).State = System.Data.Entity.EntityState.Modified;
                    myService.UpdatePersonHobbies(model.Hobbieslist, personToUpdate);
                    myService.dbSevicecontext.SaveChanges();
                    //dbcontext.SaveChanges();

                    return RedirectToAction("Index");
                }

                catch (System.Data.DataException /* dex */)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }
            else
            {
                return View(model);
            }

            // return Result(model);
            return RedirectToAction("Index", "Person", model);


        }


        public ActionResult Create()
        {
            populatedropdown();
            populatelist();
            PersonModel model = new PersonModel();

            return View(model);
        }
        [HttpPost]
        public ActionResult Create(PersonModel model)
        {

            populatedropdown();
            populatelist();
            if (ModelState.IsValid)
            {
                Person person = new Person();
                person.FirstName = model.FirstName;
                person.SurName = model.SurName;
                person.CityID = model.Cityid;
                person.Email = model.Email;
                person.Address = model.Address;
                person.Gender = model.Gender;
                person.Phoneno = model.Phoneno;
                person.Hobbies = new List<Hobby>();


                myService.dbSevicecontext.Persons.Add(person);
                myService.dbSevicecontext.SaveChanges();
                myService.AddPersonHobbies(model.Hobbieslist, person);
                //AddPersonHobbies(model.Hobbieslist,person);
                //dbcontext.SaveChanges();
                myService.dbSevicecontext.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }

        }
        public void populatedropdown()
        {
            var City = from d in myService.dbSevicecontext.Cities
                       select d;
            ViewBag.DropDown = new SelectList(City, "ID", "Name");
        }
        public void populatelist()
        {
            var Hobby = from d in myService.dbSevicecontext.Hobbies
                        select d;

            ViewBag.List = new SelectList(Hobby, "ID", "Name");
        }
    }
}