﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AbsarLib.Entity;

namespace MVC.Models
{
    public class PersonModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "You forgot to enter a first name.")]
        [StringLength(160, MinimumLength = 3)]
        public String FirstName { get; set; }

        [StringLength(160, MinimumLength = 3)]
        [Required]
        public String SurName { get; set; }
        [Required]
        public String Address { get; set; }
        [Required]
        public String Gender { get; set; }
        [Required]
        [StringLength(11, MinimumLength = 11, ErrorMessage = "Mobile Number is invalid")]
        //[RegularExpression("([1-9][0-9]*)",ErrorMessage="only digits allow in phone number")]
        public String Phoneno { get; set; }
        [Required]
        //[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Email doesn't look like a valid email address.")]
        public String Email { get; set; }
    
        public String City { get; set; }
        [Required]
        public int Cityid { get; set; }
        public String Hobbies{ get; set; }
        [Required]
        public List<int> Hobbieslist { get; set; }
       
        
    }
}