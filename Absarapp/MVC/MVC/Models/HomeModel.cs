﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AbsarLib.Entity;
using PagedList;

namespace MVC.Models
{
    public class HomeModel
    {
        public IPagedList<PersonOutModel> Persons { get; set; }

        public string SearchString { get; set; }
        public string CurrentFilter { get; set; }
        public string NameSortParm { get; set; }
        public string SortOrder { get; set; }
        public string CurrentSort { get; set; }
        public int? Page { get; set; }
    }

    public class PersonOutModel
    {
        public int ID { get; set; }
        public String FirstName { get; set; }
        public String SurName { get; set; }
        public String Address { get; set; }
        public String Gender { get; set; }
        public String Phoneno { get; set; }
        public String Email { get; set; }
        public string City { get; set; }
        public List<string> Hobbies { get; set; }
    }
}