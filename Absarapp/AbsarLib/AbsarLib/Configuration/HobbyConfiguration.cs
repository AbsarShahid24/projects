﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using AbsarLib.Entity;

namespace AbsarLib.DAL
{
    class HobbyConfiguration : EntityTypeConfiguration<Hobby>
    {
        public HobbyConfiguration()
        {
            ToTable("tbl_hobby");

            Property(t => t.ID)
               .HasColumnName("id")
               .IsRequired();
         Property(t => t.Name)
            .HasColumnName("name")
            .IsRequired();

        }
    }
}
