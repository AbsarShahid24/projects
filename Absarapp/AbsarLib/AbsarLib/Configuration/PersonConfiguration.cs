﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using AbsarLib.Entity;

namespace AbsarLib.DAL
{
    class PersonConfiguration : EntityTypeConfiguration<Person>
    {
        public PersonConfiguration()
        {
            ToTable("tbl_person");

            Property(t => t.ID)
                  .HasColumnName("id");
            HasKey(p => p.ID);

            Property(t => t.FirstName)
                  .HasColumnName("firstname")
                  .IsRequired();
            Property(t => t.SurName)
               .HasColumnName("surname")
               .IsRequired();
            Property(t => t.Address)
               .HasColumnName("address")
               .IsRequired();
            Property(t => t.CityID)
               .HasColumnName("city_id")
               .IsRequired();
            Property(t => t.Email)
               .HasColumnName("email")
               .IsRequired();
            Property(t => t.Gender)
               .HasColumnName("gender")
               .IsRequired();
            Property(t => t.Phoneno)
               .HasColumnName("phoneno")
               .IsRequired();

            HasMany(p => p.Hobbies).WithMany(h => h.Persons)
            .Map(t => t.MapLeftKey("person_id")
                .MapRightKey("hobby_id")
                .ToTable("tbl_personhobby"));
        }
    }
}
