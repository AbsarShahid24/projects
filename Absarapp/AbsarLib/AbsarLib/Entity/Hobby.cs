﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsarLib.Entity
{
   public class Hobby
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public virtual ICollection<Person> Persons { get; set; }
    }
}
