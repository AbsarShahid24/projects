﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace AbsarLib.Entity
{
   public class Person
    {
       [Key]
        public int ID { get; set; }
        
        public String FirstName { get; set; }

        
        public String SurName { get; set; }
        
        public String Address { get; set; }
        
        public String Gender { get; set; }
                
        public String Phoneno { get; set; }
        
       
        public String Email { get; set; }
        public int CityID { get; set; }
        public virtual City City { get; set; }
        public virtual ICollection<Hobby> Hobbies { get; set; }
    }
}
