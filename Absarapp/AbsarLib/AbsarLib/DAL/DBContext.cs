﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsarLib.Entity;
using System.Data.Entity;
namespace AbsarLib.DAL
{
  public class DBContext:DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Hobby> Hobbies { get; set; }
        public DBContext()
            : base("name=DBContext")
        {
            Database.SetInitializer<DBContext>(new Initializer());
           


        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Configurations.Add(new PersonConfiguration());
            modelBuilder.Configurations.Add(new CityConfiguration());
            modelBuilder.Configurations.Add(new HobbyConfiguration());
            
        }
    }
}
