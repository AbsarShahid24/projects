﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsarLib.Entity;

namespace AbsarLib.DAL
{
    class Initializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<DBContext>
    {
       protected override void Seed(DBContext context)
        {
            var cities = new List<City>
            {
           new City{ID=1,Name="Rawalpindi"},
            new City{ID=2,Name="Sialkot"},
            new City{ID=3,Name="LAhore"},

            };

            cities.ForEach(s => context.Cities.Add(s));
            context.SaveChanges();
            var hobbies = new List<Hobby>
            {
           new Hobby{ID=1,Name="Cricket"},
            new Hobby{ID=2,Name="Gaming"},
            new Hobby{ID=3,Name="Football"},
            new Hobby{ID=4,Name="Tennis"}

            };

            hobbies.ForEach(s => context.Hobbies.Add(s));
            context.SaveChanges();
            var persons = new List<Person>
            {
           new Person{ID=1,FirstName="Ali",SurName="Bukhari",CityID=1,Address="Sector 3 house 35",Gender="Male", Phoneno="03365600828",Email="abc@yahoo.com",Hobbies=new List<Hobby>() },
            new Person{ID=2,FirstName="Zahid",SurName="Khan",CityID=1,Address="Sector 2 house 24",Gender="Male", Phoneno="03335169828",Email="xyz@yahoo.com",Hobbies=new List<Hobby>()},
            new Person{ID=3,FirstName="Tahir",SurName="Rizvi",CityID=2,Address="Sector 4 house 13",Gender="Male" ,Phoneno="03005673828",Email="qwe@yahoo.com",Hobbies=new List<Hobby>()},
            new Person{ID=4,FirstName="Khalid",SurName="Lateef",CityID=3,Address="Sector 6 house 67",Gender="Male", Phoneno="03455762828",Email="tyu@yahoo.com",Hobbies=new List<Hobby>() }
            
            

            };

            persons.ForEach(s => context.Persons.Add(s));
            context.SaveChanges();
            AddOrUpdateHobby(context, 1, "Football");
            AddOrUpdateHobby(context, 1, "Cricket");
            AddOrUpdateHobby(context, 2, "Cricket");
            AddOrUpdateHobby(context, 3, "Cricket");
            AddOrUpdateHobby(context, 4, "Cricket");
            context.SaveChanges();
        }
       void AddOrUpdateHobby(DBContext context, int personId, string Hobby)
       {
           var per = context.Persons.SingleOrDefault(p => p.ID == personId);
           var hby = per.Hobbies.SingleOrDefault(h => h.Name == Hobby);
           if (hby == null)
               per.Hobbies.Add(context.Hobbies.Single(h => h.Name == Hobby));
       }
    }
}