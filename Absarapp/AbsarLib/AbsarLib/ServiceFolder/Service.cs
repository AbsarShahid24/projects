﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsarLib.DAL;
using AbsarLib.Entity;
namespace AbsarLib.ServiceFolder
{
    public class Service
    {
       public DBContext dbSevicecontext = new DBContext();
        public IQueryable<Person> Search(String SearchString)
        {
            var person = dbSevicecontext.Persons.Where(s => (SearchString == null || s.FirstName.Contains(SearchString))
                                       || (SearchString == null || s.SurName.Contains(SearchString)));
           return person;
        }
        public void AddPersonHobbies(List<int> selectedHobbies, Person PersonToUpdate)
        {
            //int[] selecteHobbiesarray = selectedHobbies.ToArray();
            if (selectedHobbies == null)
            {
                PersonToUpdate.Hobbies = new List<Hobby>();
                return;
            }

            var selectedHobbiesHS = new HashSet<int>(selectedHobbies);

            foreach (var hobby in dbSevicecontext.Hobbies)
            {
                if (selectedHobbiesHS.Contains(hobby.ID))
                {

                    PersonToUpdate.Hobbies.Add(hobby);

                }

            }
            dbSevicecontext.SaveChanges();
        }
        public void UpdatePersonHobbies(List<int> selectedHobbies, Person PersonToUpdate)
        {
            //int[] selecteHobbiesarray = selectedHobbies.ToArray();
            if (selectedHobbies == null)
            {
                PersonToUpdate.Hobbies = new List<Hobby>();
                return;
            }

            var selectedHobbiesHS = new HashSet<int>(selectedHobbies);
            var personHobbies = new HashSet<int>
                (PersonToUpdate.Hobbies.Select(h => h.ID));
            foreach (var hobby in dbSevicecontext.Hobbies)
            {
                if (selectedHobbiesHS.Contains(hobby.ID))
                {
                    if (!personHobbies.Contains(hobby.ID))
                    {
                        PersonToUpdate.Hobbies.Add(hobby);
                    }
                }
                else
                {
                    if (personHobbies.Contains(hobby.ID))
                    {
                        PersonToUpdate.Hobbies.Remove(hobby);
                    }
                }
            }
            dbSevicecontext.SaveChanges();
        }
    }
}
